# Rubik_Cube_3D
<p>This project takes input from a scambled rubik cube and provides you the solution in 3D model</p>
<h2>Demo</h2>
<img src="https://gitlab.com/vaibhav-ankolekar/Rubik_Cube_3D/-/raw/main/screenshots/Rubik%20Cube%20Demo.gif" width="600px">
<h2>Libraries used</h2>
<li>OpenCV</li>
<li>rubik_solver</li>
<li>Ursina</li>

